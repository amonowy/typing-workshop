from typing import List, Dict, Tuple, Callable, Union, Any, Optional, Type, TypeVar, NewType

# available generic types
List[int]
Dict[int, str]
Tuple[str, int], Tuple[dict, ...]
Union[str, int]  # either this or that


# some custom defined class with type annotation
class Foo:
    pass


def get_foo() -> Foo:  # this is how it works with custom classes
    return Foo()


# some example typing with Union
def echo(a: Union[str, int]) -> None:
    print(a)


echo(1)
echo("1")
echo({1, 2})
