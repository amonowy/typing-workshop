# some module in asset_models.py
something = [1, 2, 3]
something = {1: 1, 2: 2, 3: 3}
something = range(1, 4)
something = {1, 2, 3}
something = (1, 2, 3)
something = "123"

# some parser in asset_parsers.py
items = [item for item in something]

# what the heck is something?
# http://3.bp.blogspot.com/-ilMjE1Gh3Yg/VpUAmd-6TWI/AAAAAAAAAbg/-FJ08zxN42s/s1600/WFTPM.png
