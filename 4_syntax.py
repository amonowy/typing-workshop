# variables
a: int = 4

# functions
def foo(_list: list, el: int) -> None:
    _list.append(el)


def foo(a: int, b: int) -> int:
    return a + b


# classes
class Foo:
    a: str
    b: set = {1, 2, 3}

# modules
