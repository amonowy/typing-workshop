# python is dynamically typed
a = "str"
a = int(4)

# so some errors pass
if False:
    4 + "4"  # this will never work
else:
    2 - 2
