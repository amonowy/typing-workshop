# Add type annotations here


class Device:
    def __init__(self, name, zms_id):
        self.name = None
        self.zms_id = None
        self.bool_params = {}
        self.int_params = {}
        self.str_params = {}

    def change(self, new_device_instance):
        self = new_device_instance

    def service(self, param, value):
        if isinstance(value, str):
            params = self.str_params
        elif isinstance(value, int):
            params = self.int_params
        elif isinstance(value, bool):
            params = self.bool_params

        params[param] = value

    def get_params(self):
        return list(
{**self.str_params, **self.bool_params, **self.int_params,}.keys()
                )

    def get_params_values(self):
        return list(
{**self.str_params, **self.bool_params, **self.int_params,}.values()
                )

