def pre_echo() -> int:
    return 10


# def pre_echo():
#     return 10


def echo(text: str) -> None:
    print(text)


result = echo(pre_echo())
